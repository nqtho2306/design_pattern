package abstract_mehod_factory;

public abstract class SecondFactory {
	
	public abstract Table getTable();
	
	public abstract Chair getChair();
	
}
