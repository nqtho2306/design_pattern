package abstract_mehod_factory;

public class FirstFactory {
	
	private FirstFactory() {
		
	}
	
	public static SecondFactory getType(ProductType productType) {
		switch(productType) {
			case PLASTIC:
				return new PlasticProduct();
			case WOOD:
				return new WoodProduct();
			default:
				throw new IllegalArgumentException();
		}
	}
}
