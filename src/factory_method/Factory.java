package factory_method;

import java.lang.IllegalArgumentException;

public class Factory {
	private Factory() {
		
	}
	
	public static final Product getProduct(TypeProduct typeProduct) {
		switch(typeProduct) {
			case TABLE:
				return new Table();
			case CHAIR:
				return new Chair();
			default:
				throw new IllegalArgumentException();
		}
	}
}
