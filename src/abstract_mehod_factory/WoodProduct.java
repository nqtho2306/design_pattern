package abstract_mehod_factory;

public class WoodProduct extends SecondFactory {

	@Override
	public Table getTable() {
		// TODO Auto-generated method stub
		return new WoodTable();
	}

	@Override
	public Chair getChair() {
		// TODO Auto-generated method stub
		return new WoodChair();
	}
	
}
